#!/usr/bin/env bash

openstack stack delete --wait -y kypo-head-stack
openstack stack delete --wait -y kypo-proxy-jump-stack
openstack stack delete --wait -y kypo-base-security-groups-stack
openstack stack delete --wait -y kypo-base-networking-stack
