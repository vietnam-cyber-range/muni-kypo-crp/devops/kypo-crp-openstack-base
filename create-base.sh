#!/usr/bin/env bash

export rule_id=`openstack security group rule list default -f value | grep icmp | awk '{print $1}'`
[[ ! -z "$rule_id" ]] && openstack security group rule delete "$rule_id"
openstack stack create --wait -e kypo-base-params.yml -t heat/kypo-base-networking.yml kypo-base-networking-stack
openstack stack create --wait -t heat/kypo-base-security-groups.yml kypo-base-security-groups-stack
openstack stack create --wait -e kypo-base-params.yml -t heat/kypo-head.yml kypo-head-stack
openstack stack create --wait -e kypo-base-params.yml -t heat/kypo-proxy-jump.yml kypo-proxy-jump-stack
