#!/usr/bin/env bash

export KYPO_HEAD_FLAVOR="standard.large"
export KYPO_HEAD_IMAGE="ubuntu-focal-x86_64"
export KYPO_HEAD_USER="ubuntu"
export KYPO_PROXY_FLAVOR="standard.medium"
export KYPO_PROXY_IMAGE="ubuntu-focal-x86_64"
export KYPO_PROXY_USER="ubuntu"
export DNS1="1.1.1.1"
export DNS2="1.0.0.1"
