#!/usr/bin/env bash

# TODO function
if [[ -z "${KYPO_HEAD_FLAVOR}" ]]; then
    echo "Variable KYPO_HEAD_FLAVOR is undefined."
    exit 1
fi

if [[ -z "${KYPO_HEAD_IMAGE}" ]]; then
    echo "Variable KYPO_HEAD_IMAGE is undefined."
    exit 1
fi

if [[ -z "${KYPO_HEAD_USER}" ]]; then
    echo "Variable KYPO_HEAD_USER is undefined."
    exit 1
fi

if [[ -z "${KYPO_PROXY_FLAVOR}" ]]; then
    echo "Variable KYPO_PROXY_FLAVOR is undefined."
    exit 1
fi

if [[ -z "${KYPO_PROXY_IMAGE}" ]]; then
    echo "Variable KYPO_PROXY_IMAGE is undefined."
    exit 1
fi

if [[ -z "${KYPO_PROXY_USER}" ]]; then
    echo "Variable KYPO_PROXY_USER is undefined."
    exit 1
fi

if [[ -z "${DNS1}" ]]; then
    echo "Variable DNS1 is undefined."
    exit 1
fi

if [[ -z "${DNS2}" ]]; then
    echo "Variable DNS2 is undefined."
    exit 1
fi

KYPO_OSTACK_EXTERNAL_NET=$1

if [[ -z "${KYPO_OSTACK_EXTERNAL_NET}" ]]; then
    echo "Please provide name of the external network."
    exit 1
fi

HEAD_TAG="kypo-base-head"
HEAD_FIP_RESPONSE=$(openstack floating ip list --format yaml --tags "${HEAD_TAG}" | yq .[0])

if [ "${HEAD_FIP_RESPONSE}" == "null" ]; then
  echo "Floating IP ${HEAD_TAG} for network ${KYPO_OSTACK_EXTERNAL_NET} does not exist. Creating..."
  HEAD_FIP=$(openstack floating ip create --format yaml --tag "${HEAD_TAG}" ${KYPO_OSTACK_EXTERNAL_NET} | yq -r .id)
else
  echo "Floating IP ${HEAD_TAG} for network ${KYPO_OSTACK_EXTERNAL_NET} exists. No action."
  HEAD_FIP=$(echo "${HEAD_FIP_RESPONSE}" | yq -r .ID)
fi

# TODO function
PROXY_TAG="kypo-base-proxy"
PROXY_FIP_RESPONSE=$(openstack floating ip list --format yaml --tags "${PROXY_TAG}" | yq .[0])

if [ "${PROXY_FIP_RESPONSE}" == "null" ]; then
  echo "Floating IP ${PROXY_TAG} for network ${KYPO_OSTACK_EXTERNAL_NET} does not exist. Creating..."
  PROXY_FIP=$(openstack floating ip create --format yaml --tag "${PROXY_TAG}" ${KYPO_OSTACK_EXTERNAL_NET} | yq -r .id)
else
  echo "Floating IP ${PROXY_TAG} for network ${KYPO_OSTACK_EXTERNAL_NET} exists. No action."
  PROXY_FIP=$(echo "${PROXY_FIP_RESPONSE}" | yq -r .ID)
fi

#### The key

KYPO_PROJECT_ID=$(openstack floating ip list --format yaml | yq -r .[0].Project)
KYPO_PROJECT_NAME=$(openstack project show --format yaml "${KYPO_PROJECT_ID}" | yq -r .name)
KYPO_KEYNAME="${KYPO_PROJECT_NAME}_kypo-base-key"
KYPO_KEYFILE="${KYPO_KEYNAME}.key"

SHOW_KEY=$(openstack keypair show --format yaml "${KYPO_KEYNAME}")
RETURN_CODE=$?

if [ $RETURN_CODE -ne 0 ]; then
  echo "Creating keypair ${KYPO_KEYNAME}."
  openstack keypair create --format yaml --private-key "${KYPO_KEYFILE}" "${KYPO_KEYNAME}" || exit 1
  chmod 0600 "${KYPO_KEYFILE}"
else
  echo "Keypair ${KYPO_KEYNAME} exists. No action."
fi

jinja2 templates-j2/kypo-base-params.j2.yml \
  -D ext_net_name="${KYPO_OSTACK_EXTERNAL_NET}" \
  -D keypair="${KYPO_KEYNAME}" \
  -D head_flavor="${KYPO_HEAD_FLAVOR}" \
  -D head_image="${KYPO_HEAD_IMAGE}" \
  -D head_fip="${HEAD_FIP}" \
  -D proxy_flavor="${KYPO_PROXY_FLAVOR}" \
  -D proxy_image="${KYPO_PROXY_IMAGE}" \
  -D dns1="${DNS1}" \
  -D dns2="${DNS2}" \
  -D proxy_fip="${PROXY_FIP}" > kypo-base-params.yml < /dev/null

#### KYPO user key

KYPO_USER_KEYFILE="${KYPO_PROJECT_NAME}_kypo-user-key.key"

if [ ! -f "${KYPO_USER_KEYFILE}" ]; then
    echo "Private key for user access does not exist. Creating..."
    openssl genrsa -out "${KYPO_USER_KEYFILE}" 2048
else
    echo "Private key for user access exists. No action."
fi

KYPO_USER_SSH_PUBKEY=$(ssh-keygen -y -f "${KYPO_USER_KEYFILE}" | base64)

#### Ansible inventory

HEAD_FIP_ADDR=$(openstack floating ip show --format yaml "${HEAD_FIP}" | yq -r .floating_ip_address)
PROXY_FIP_ADDR=$(openstack floating ip show --format yaml "${PROXY_FIP}" | yq -r .floating_ip_address)

jinja2 templates-j2/ansible_hosts.j2.yml \
  -D head_fip_addr="${HEAD_FIP_ADDR}" \
  -D head_user="${KYPO_HEAD_USER}" \
  -D proxy_fip_addr="${PROXY_FIP_ADDR}" \
  -D proxy_user="${KYPO_PROXY_USER}" \
  -D keyfile="./${KYPO_KEYFILE}" \
  -D kypo_user_pubkey_b64="${KYPO_USER_SSH_PUBKEY}" > provisioning/ansible_hosts.yml < /dev/null
